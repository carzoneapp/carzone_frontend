import React, { useState } from 'react';
import './category.css';

import { FaMapMarkerAlt } from "react-icons/fa";
import { useNavigate } from 'react-router-dom';

const Category = () => {
    const navigate = useNavigate()

    const [currentCatIdx, setcurrentCatIdx] = useState(0);
    


    const categories = [
        {
            title: 'Budget',
            subTitle: 'Under 3 lakh, 3-5lakh...',
            subCategory: [
                {
                    title: 'Under',
                    value: '3 lakh'
                },
                {
                    title: 'From',
                    value: '3 lakh - 5 lakh'
                },
                {
                    title: 'From',
                    value: '5 lakh - 7 lakh'
                },
                {
                    title: 'From',
                    value: '7 lakh - 10 lakh'
                },
                {
                    title: 'Above',
                    value: '10 lakh'
                },
            ]
        },
        {
            title: 'Year',
            subTitle: '2020-22, 2018-20...',
            subCategory: [
                {
                    title: '2024',
                    value: 'and newer'
                },
                {
                    title: 'Between',
                    value: '2020 to 2023'
                },
                {
                    title: 'Between',
                    value: '2016-2019'
                },
                {
                    title: '2015',
                    value: 'and older'
                },
            ]
        },
        {
            title: 'Body type',
            subTitle: 'Hatchback, Sedan...',
            subCategory: [
                {
                    image: 'Hatchback',
                    value: 'Hatchback'
                },
                {
                    image: 'SUV',
                    value: 'SUV'
                },
                {
                    image: 'Sedan',
                    value: 'Sedan'
                },
                {
                    image: 'Luxury suv',
                    value: 'Luxury suv'
                },
                {
                    image: 'Luxury sedan',
                    value: 'Luxury sedan'
                },
            ]
        },
        {
            title: 'Fuel type',
            subTitle: 'Petrol, Diesel...',
            subCategory: [
                {
                    image: 'Petrol',
                    value: 'Petrol'
                },
                {
                    image: 'Diesel',
                    value: 'Diesel'
                },
                {
                    image: 'LPG',
                    value: 'LPG'
                },
                {
                    image: 'CNG',
                    value: 'CNG'
                },
            ]
        },
    ]

    return (
        <div className="category_container">
            <h2>Cars by category</h2>
            <div className='category_list_container' >
                {categories?.map((category, categoryIdx) => (
                    <div onClick={() => {setcurrentCatIdx(categoryIdx)}} className={`category_list_item ${currentCatIdx == categoryIdx ? 'active_category' : null} `} >
                        <div className='category_list_item_title' >{category?.title}</div>
                        <div className='category_list_item_subtitle'>{category?.subTitle}</div>
                    </div>
                ))}
            </div>

            <div>
                {categories?.map((category, categoryIdx) => (
                    <div >
                        {currentCatIdx == categoryIdx && (
                            <div className='subcategory_list_container' >
                                {category?.subCategory?.map((subItem, subIdx) => (
                                    <div className='subcategory_list_item' 
                                        onClick={() => {navigate('/details');}}
                                    >
                                        <div className='subcategory_list_item_title' >{subItem?.title}</div>
                                        <div className='subcategory_list_item_subtitle' >{subItem?.value}</div>
                                    </div>
                                ))}
                            </div>
                        )}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Category;
