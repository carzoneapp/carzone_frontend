import React, { useState } from 'react';
import './featured.css';

import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { productData, responsive } from '../../../global';
import { FaMapMarkerAlt } from "react-icons/fa";

const FeaturedList = () => {
    const [currentIndex, setCurrentIndex] = useState(0);

    


    const product = productData.map((val) => {

        const item = {
            url: 'https://fastly-production.24c.in/hello-ar/dev/uploads/ceacc62b-e6da-4559-a23e-046da0500f20/585f1153-4eda-4a36-be54-0f6ab8c63d0e/44.jpg?w=700&h=403&auto=format',
            make: "Ford",
            model: "Mustang",
            year: 2023,
            color: "Black",
            engine: "5.0L V8",
            transmission: "Automatic",
            features: ["Navigation System", "Leather Seats", "Premium Sound System"],
            price: 1316000,
            offer_price: 1264000,
            emi: 24711,
            offer_emi: 24059,
            mileage: 5000,
            down_payment: 0,
            location: 'GT World Mall, Bangalore',
            vin: "1FA6P8CF3F1234567",
        }

        return(
            <div className="feature_card">
                <img className="feature_card_img" src={item.url} alt="product image" />
                <div className='feature_card_content' >
                    <div className='row_style' >
                        <div className='card_title' >{item.year}</div>
                        <div className='card_title' >{item.model}</div>
                    </div>
                    <div className='row_style' >
                        <div className='card_subtitle'>{item.engine}</div>
                        <div className='bullet'>{'\u2B24'}</div>
                        <div className='card_subtitle'>{item.transmission}</div>
                    </div>
                    <div className='feature_card_features' >
                        {item?.features?.map((item, index) => (
                            <div className='feature_card_featureItem' >{item}</div>
                        ))}
                    </div>
                    <div className='row_style space_between' >
                        <div className='flex_start' >
                            <div className='card_title' >{'\u20B9'} {item.offer_emi}/month</div>
                            <div className='card_subtitle text_decoration' >{'\u20B9'} {item.emi}/month</div>
                        </div>
                        <div className='flex_start' >
                            <div className='card_title' >{'\u20B9'} {item.offer_price}</div>
                            <div className='card_subtitle text_decoration' style={{ justifyContent: 'flex-end', width: '100%', display: 'flex', }} >{'\u20B9'} {item.price}</div>
                        </div>
                    </div>
                    <div className='card_bottom_container' >
                        <div className='card_down_payment'>Zero {item.down_payment} down payment</div>
                        <div className='row_style' >
                            <FaMapMarkerAlt size={14} />
                            <div className='card_location'>{item.location}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div className="featured-cars">
           <Carousel showDots={true} responsive={responsive} className='carousel' >
                {product}
            </Carousel>
            
        </div>
    );
};

export default FeaturedList;
