import React, { useState, useEffect, useRef } from 'react';
import './banner.css';
import { Link } from 'react-router-dom';

const HomeBanner = () => {
    const [isPopupVisible, setIsPopupVisible] = useState(false);
    const searchBarRef = useRef(null);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (searchBarRef.current && !searchBarRef.current.contains(event.target)) {
                setIsPopupVisible(false);
            }
        };

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    const handleFocus = () => {
        setIsPopupVisible(true);
    };

    return (
        <div className="home-banner">
            <h1 className="banner-title">Find Your Perfect Car</h1>
            <div className="search-bar" ref={searchBarRef}>
                <input
                    type="text"
                    placeholder="Search for cars, brands, etc."
                    onFocus={handleFocus}
                />
                {isPopupVisible && (
                    <div className="search_popup">
                        <h2 className="search_popup-title">Trending Cars</h2>
                        <ul className="search_popup-list">
                            <li className="search_popup-item">Cardsadsadsa 1</li>
                            <li className="search_popup-item">Car 2</li>
                            <li className="search_popup-item">Cardsadsadsa 3</li>
                            <li className="search_popup-item">Car 4</li>
                            <li className="search_popup-item">Cardsadsadsa 5</li>
                            <li className="search_popup-item">Car 6</li>
                            <li className="search_popup-item">Cardsadsadsa 7</li>
                            <li className="search_popup-item">Cardsadsadsa 8</li>
                            <li className="search_popup-item">Car 9</li>
                            <li className="search_popup-item">Car 10</li>
                        </ul>
                    </div>
                )}
            </div>
            <div className="brands_container">
                <div className='brand_item_container' >
                    <div className="brand">Brand 1</div>
                    <div className="brand">Brand 2</div>
                    <div className="brand">Brand 3</div>
                    <div className="brand">Brand 4</div>
                    <div className="brand">Brand 5</div>
                </div>
                <Link to="/all-brands">
                    <button className="view-all-button">View All</button>
                </Link>
            </div>
        </div>
    );
};

export default HomeBanner;
