import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';

import { IoMdArrowDropdown } from "react-icons/io";
import { MdFavoriteBorder, MdMenu } from "react-icons/md";
import { FaRegCircleUser } from "react-icons/fa6";

const Navbar = () => {
    const [menuOpen, setMenuOpen] = useState(false);

    const toggleMenu = () => {
        setMenuOpen(!menuOpen);
    };

    return (
        <nav className="navbar">
            <div className="navbar-brand">
                <Link to="/">Car Zone</Link>
            </div>
            <div className="navbar-links">
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                </ul>
            </div>
            <div className="icon-container">
                <MdFavoriteBorder size={24} />
                <div className="login-container">
                    <button className="login-button">
                        <div className="user-avatar">
                            <FaRegCircleUser size={22} />
                        </div>
                        <div className="user-info">
                            <div className="user-title">Hello, Sign in</div>
                            <div className="user-subtitle">Account</div>
                        </div>
                        <div className="chevron">
                            <IoMdArrowDropdown size={24} />
                        </div>
                    </button>
                    <div className="popup">
                        <div className="popup-content">
                            <Link to="#" className="popup-link">My Appointments</Link>
                            <Link to="#" className="popup-link">My Bookings</Link>
                            <Link to="#" className="popup-link">FAQ</Link>
                            <Link to="#" className="popup-link">Help</Link>
                        </div>
                    </div>
                </div>
                <button className="menu-button" onClick={toggleMenu}>
                    <MdMenu size={24} />
                </button>
            </div>
            {menuOpen && (
                <div className="mobile-menu">
                    <ul>
                        <li>
                            <Link to="/" onClick={toggleMenu}>Home</Link>
                        </li>
                        <li>
                            <Link to="/about" onClick={toggleMenu}>About</Link>
                        </li>
                    </ul>
                </div>
            )}
        </nav>
    );
};

export default Navbar;
