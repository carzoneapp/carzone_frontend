import React from 'react';
import { BrowserRouter as Router, Route, Routes,  } from 'react-router-dom';

import HomePage from './pages/home';
import CarDetails from './pages/cars';
import Navbar from './components/navbar';

import './App.css'

const App = () => {
    return (
        <Router>
            <Navbar />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/details" element={<CarDetails />} />
                <Route path="/about" element={HomePage} />
                <Route path="/contact" element={HomePage} />
            </Routes>
        </Router>
    );
};

export default App;
