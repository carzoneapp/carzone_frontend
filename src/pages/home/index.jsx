import React from 'react'
import './home.css'

import { Link } from 'react-router-dom'
import HomeBanner from '../../components/home/banner'
import FeaturedList from '../../components/home/featured'
import Category from '../../components/home/category'


const HomePage = () => {

    return (
        <div className='home_container' >
            <HomeBanner />
            <FeaturedList />
            <Category />
            <h1>dsad</h1>
        </div>
    )
}

export default HomePage